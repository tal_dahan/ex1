#include "Queue.h"
#include<iostream>
using namespace std;

/*
Function get Queue and new number and add the new number to the end of the queue(if the queue is'nt full)
input:q-queue and positive number
output:non
*/
void enqueue(queue* q, unsigned int newValue)
{
	if (q->arr == 0)
	{
		std::cout << "\n Queue is undefind!" << std::endl;
	}
	//check if the Queue is'nt full
	else if (q->back == q->max)
	{
		std::cout << "\n Queue is full!" << std::endl;
	}
	//check if there is'nt objects in the Queue
	else if (q->back == -1 && q->front == -1)
	{
		//set front and back
		q->front = 0;
		q->back = 0;
		//insert the value to the Queue
		q->arr[q->back] = newValue;
	}
	else
	{
		//redirect the back value 
		q->back = q->back + 1;
		//insert the value to the Queue
		q->arr[q->back] = newValue;
	}
}

// return element in top of queue, or -1 if empty
int dequeue(queue* q)
{
	//cheak if the Queue is empty
	if (q->back == -1 && q->front == -1)
	{
		return -1;
	}
	//cheak if there is only 1 element in queue
	else if (q->front == q->back)
	{
		//set  the front and back to -1 (like empty queue) and and return the element in the top of the queue
		q->front = -1;
		q->back = -1;
		return q->arr[0];
	}
	else
	{	
		//save the first insex
		unsigned int top = q->arr[q->front];
		
		unsigned int i = 1;
		unsigned int next = 0;
		//move every number in the arr -1 index in the array and remove the value of the first index (1,2,3,4->2,3,4,-)
		while (i <= q->back)
		{
			next = q->arr[i];
			q->arr[i-1] = next;
			i++;
		}
		//set the back -1 index in the array
		q->back = q->back - 1;
		//return the  element in the top of the queue  befor the transfer
		return top;
	}

}

/*
Function recive queue and size ,if the size is valid (size>0) the function creat arr in the lengh of size
input:q-queue,size
*/
void initQueue(queue* q, unsigned int size)
{
	//cheak if the size is valid
	if (size <= 0)
	{
		std::cout << "\nsize is invalid!" << std::endl;
	}
	//creat the arr win the lengh of size
	else
	{
		q->arr = (unsigned int*)malloc(size * sizeof(unsigned int));
		//set back and front 
		q->back = -1;
		q->front = -1;
		//set q->max
		q->max = size - 1;
	}
}
/*
Function recive queue and reset all the values to clean queue
input:q-queue
output:non
*/
void cleanQueue(queue* q)
{
	//reset the array
	unsigned int *i =0;
	q->arr = i;
	//reset the front and back 
	q->front = -1;
	q->back = -1;
}

