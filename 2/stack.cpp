#include "LinkedList.h"
#include "stack.h"
#include<iostream>


/*
The function add a node to the stack 
input:s-stack,element
*/
void push(stack* s, unsigned int element)
{
	//send the list of the stack and the value and add new node
	pushNode(&(s->stack), element);
	//set top
	s->top = s->stack->val;
}

/*
function remove the first node from the stack and return is value
input:s-stack
output:the value if the first node
*/
int pop(stack* s)
{
	//return -1 if the stack is empty
	if (s->stack == NULL)
	{
		return -1;
	}
	//save the value
	int value = s->stack->val;
	//delete the first node
	deleteHead(&(s->stack));
	//set top
	if (s->stack != NULL)
	{
		s->top = s->stack->val;
	}
	//return the value
	return value;
}


void initStack(stack* s)
{
	//there is nothing we need to init the stack with - all the ditails that we need is in the struct
}

/*
Function clean all the ditails in the stack
input:s- stack
output:non
*/
void cleanStack(stack* s)
{
	s->top = 0;
	//got over the stack
	while (s->stack->next)
	{
		//delet the first node in the stack
		deleteHead(&(s->stack));
		s->stack = s->stack->next;
	}
	//delet the last node that stay in the stack
	deleteHead(&(s->stack));
}