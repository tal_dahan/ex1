#include "LinkedList.h"
#include<iostream>




/*
Function creat a new node and place it first in the LinkedList
input:head-the first node of the linkedList , value
output:non
*/
void pushNode(struct Node** head, unsigned int value)
{
	/*allocate node */
	struct Node* new_node = (struct Node*) malloc(sizeof(struct Node));

	/*put in the data  */
	new_node->val = value;

	/*Make next of new node as head */
	new_node->next = (*head);

	/* move the head to point to the new node */
	(*head) = new_node;
}

/*
Function delete the first node from the list
input:head- first node in the list
output:non
*/
void deleteHead(Node **head)
{
	//save the second  node
	Node *temp = *head;
	temp = temp->next;
	//set the head to the second node
	*head = temp;
}

