


/*node struct*/
typedef struct Node 
{
	unsigned int val;
	struct Node * next;

} Node;

//add new node to the head of the list
void pushNode(struct Node** head, unsigned int value);
//delete the first node from the list
void deleteHead(Node **head);

#pragma once
