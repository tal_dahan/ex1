#ifndef STACK_H
#define STACK_H
#include "LinkedList.h"

/* a positive-integer value stack, with no size limit */
typedef struct stack
{
	unsigned top;
	Node *stack;

} stack;


void push(stack* s, unsigned int element);
int pop(stack* s); // Return -1 if stack is empty

void initStack(stack* s);
void cleanStack(stack* s);

#endif // STACK_H