#include "utils.h"
#include "LinkedList.h"
#include "stack.h"
#include<iostream>

using namespace std;

		
/*
Function get an array and rrevers him -1,2,3,4 -> 4,3,2,1
intput:num-arr,size
output:non
*/
void reverse(int* nums, unsigned int size)
{
	//creat new stack
	stack *s = new stack();
	
	int i = 0;
	//go over the array and insert the values to the stack
	for (i; i < size; i++)
	{
		push(s, nums[i]);
	}
	i = 0;
	//got again over the array and insert the value from the top of  the stuck(that will change the order of the array values)
	for (i; i < size; i++)
	{
		nums[i] = pop(s);
	}
}

/*
Function recive 10 numbers and creat array when the first value that recived is last and the last that recived is first
input:non
output:arr of int
*/
int* reverse10()
{
	int i = 0;
	int value = 0;
	//creat a new stack
	stack *s = new stack();
	//creat a new array
	int * numbers = new int[10];
	cout << "Please enter 10 values\n ";
	for (i; i < 10; i++)
	{
		//recive the value
		cin >> value;
		//push the values to the stack
		push(s, value);
	}
	i = 0;
	//insert the values to the array
	for (i; i < 10; i++)
	{
		numbers[i] = pop(s);
	}
	//return the array
	return numbers;
}